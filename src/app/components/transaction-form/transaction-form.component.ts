import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TransactionService} from "../../services/transaction.service";
import {Subscription} from "rxjs";
import {ContactService} from "../../services/contact.service";
import {Contact} from "../../models/contact";
import {Message, MessageService} from "primeng/api";

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.scss'],
  providers: [MessageService]
})
export class TransactionFormComponent implements OnInit, OnDestroy {

  transactionForm!: FormGroup;
  selectedContact: any = null;
  contacts: Contact[] = [];
  contactsSubscription = new Subscription();

  constructor(private formBuilder: FormBuilder,
              private transactionService: TransactionService,
              private contactService: ContactService) {
  }

  ngOnInit(): void {
    this.initForm();
    this.contactsSubscription = this.contactService.contactsSubject.subscribe((contacts) => {
      this.contacts = contacts
    })
    this.contactService.emitContactsSubject()
    this.contactService.getAllContacts()
  }

  private initForm() {
    this.transactionForm = this.formBuilder.group({
      receiverUserId: ['', [Validators.required, Validators.min(1)]],
      amount: ['', [Validators.required, Validators.min(1)]],
      description: ['', [Validators.pattern('[a-zA-Z0-9 -]{0,250}')]]
    })
  }

  onSubmitForm() {

    this.transactionService.addTransaction(this.transactionForm);
    this.transactionForm.reset();
  }

  ngOnDestroy() {
    this.contactsSubscription.unsubscribe()
  }
}
