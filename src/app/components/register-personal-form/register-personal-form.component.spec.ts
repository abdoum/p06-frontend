import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterPersonalFormComponent } from './register-personal-form.component';

describe('RegisterPersonalFormComponent', () => {
  let component: RegisterPersonalFormComponent;
  let fixture: ComponentFixture<RegisterPersonalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterPersonalFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterPersonalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
