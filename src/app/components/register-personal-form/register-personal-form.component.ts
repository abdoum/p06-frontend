import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {MustMatch} from "../../validators/MustMatch";

@Component({
  selector: 'app-register-personal-form',
  templateUrl: './register-personal-form.component.html',
  styleUrls: ['./register-personal-form.component.scss']
})
export class RegisterPersonalFormComponent implements OnInit {

  submitted: boolean = false;
  registerForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.registerForm = this.formBuilder.group({
      firstName: [this.authService.registrationRequest.firstName?.toUpperCase() || '', [Validators.required, Validators.pattern('[A-Za-z-' +
        '\\s]{2,15}')]],
      lastName: [this.authService.registrationRequest.lastName?.toUpperCase() || '', [Validators.required, Validators.pattern('[A-Za-z-' +
        '\\s]{2,15}')]],
    })
  }

  nextPage() {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.registrationRequest.firstName = this.registerForm.get("firstName")?.value
    this.authService.registrationRequest.lastName = this.registerForm.get("lastName")?.value
    this.router.navigate(['register/bank']);
    this.submitted = true;
  }

  prevPage() {
    this.router.navigate(['register/credentials'])
  }
}
