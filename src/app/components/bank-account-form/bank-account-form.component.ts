import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-bank-account-form',
  templateUrl: './bank-account-form.component.html',
  styleUrls: ['./bank-account-form.component.scss']
})
export class BankAccountFormComponent implements OnInit {

  submitted: boolean = false;
  registerForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.registerForm = this.formBuilder.group({
      iban: [this.authService.registrationRequest.bankAccount.iban || '', [Validators.required, Validators.pattern('\\b[A-Z]{2}[0-9]{2}(?:[ ]?[0-9]{4}){4}(?!(?:[ ]?[0-9]){3})(?:[ ]?[0-9]{1,2})?\\b'
      )]],
      swiftCode: [this.authService.registrationRequest.bankAccount.swiftCode || '', [Validators.required, Validators.pattern('^[A-Z]{6}[A-Z0-9]{2}([A-Z0-9]{3})?$')]],
      ownerFullName: [`${this.authService.registrationRequest.firstName?.toUpperCase()} ${this.authService.registrationRequest.lastName?.toUpperCase()}` ||
      '', [Validators.required, Validators.pattern('[A-Za-z-\\s]{4,30}')]],
    })
  }

  nextPage() {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.registrationRequest.bankAccount.iban = this.registerForm.get("iban")?.value;
    this.authService.registrationRequest.bankAccount.swiftCode = this.registerForm.get("swiftCode")?.value
    this.authService.registrationRequest.bankAccount.ownerFullName = this.registerForm.get("ownerFullName")?.value
    this.router.navigate(['register/confirmation']);
    this.submitted = true;
  }

  prevPage() {
    this.router.navigate(['register/personal']);
  }
}
