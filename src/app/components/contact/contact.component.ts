import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContactService} from "../../services/contact.service";
import {Subscription} from "rxjs";
import {Contact} from "../../models/contact";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConfirmationService, MessageService} from "primeng/api";


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class ContactComponent implements OnInit, OnDestroy {
  contacts: Contact[] = [];
  contactsSubscription = new Subscription();
  newContactSubscription = new Subscription();
  contactForm !: FormGroup;
  selectedContacts: Contact[] = [];
  contactDialog = false;
  newContact: Contact | any;
  loading = false;
  errorMessage = '';
  totalRecords!: number;

  constructor(private messageService: MessageService,
              private contactService: ContactService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.loading = true
    this.initForm()
    this.contactsSubscription = this.contactService.contactsSubject.subscribe((contacts) => {
      this.contacts = contacts;
    })
    this.newContactSubscription = this.contactService.newContactSubject.subscribe((contact) => {
      this.newContact = contact
      this.hideDialog()
      this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Contact added', life: 3000});
    })
    this.contactService.emitContactsSubject()
    this.contactService.getAllContacts()
    this.totalRecords = this.contacts.length
    this.loading = false
  }

  openNew() {
    this.newContact = {};
    this.contactDialog = true;
  }

  ngOnDestroy() {
    this.contactsSubscription.unsubscribe()
    this.newContactSubscription.unsubscribe()
  }

  private initForm() {
    this.contactForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    })
  }

  onSubmitForm() {
    this.contactService.addContact(this.contactForm);
  }

  hideDialog() {
    this.contactDialog = false;
    this.newContact = {};
    this.contactForm.reset()
  }
}
