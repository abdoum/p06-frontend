import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCredentialsForm } from './register-credentials-form.component';

describe('RegisterComponent', () => {
  let component: RegisterCredentialsForm;
  let fixture: ComponentFixture<RegisterCredentialsForm>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCredentialsForm ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCredentialsForm);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
