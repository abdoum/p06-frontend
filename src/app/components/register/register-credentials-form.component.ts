import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Message} from "primeng/api";
import {AuthService} from "../../services/auth.service";
import {MustMatch} from "../../validators/MustMatch";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register-credentials-form.component.html',
  styleUrls: ['./register-credentials-form.component.scss']
})
export class RegisterCredentialsForm implements OnInit {

  registerForm!: FormGroup;
  messages: Message[] = [];
  submitted: boolean = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.registerForm = this.formBuilder.group({
      username: [this.authService.registrationRequest.username || '', [Validators.required, Validators.email]],
      password: [this.authService.registrationRequest.password || '', [Validators.required, Validators.pattern('[A-Za-z\\d]{6,25}')]],
      passwordConfirmation: [this.authService.registrationRequest.passwordConfirmation || '', [Validators.pattern('[A-Za-z\\d]{6,25}')]]
    }, {
      validator: MustMatch('password', 'passwordConfirmation')
    })
  }

  nextPage() {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.registrationRequest.username = this.registerForm.get("username")?.value
    this.authService.registrationRequest.password = this.registerForm.get("password")?.value
    this.authService.registrationRequest.passwordConfirmation = this.registerForm.get("passwordConfirmation")?.value
    this.router.navigate(['register/personal']);
    this.submitted = true;
  }
}
