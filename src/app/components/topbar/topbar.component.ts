import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  @Input() title: string = "Pay My Buddy";
  @Input() isAuth: boolean = false;
  isLoginPage: boolean | undefined;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        this.isLoginPage = event.urlAfterRedirects === "/login";
      }
      });
  }

  onLogOff() {
    this.authService.logOff()
  }
}
