import {Component, Input, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {MenuItem} from "primeng/api";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  readonly home = {icon: 'pi pi-home', routerLink: '/'};
  menuItems: MenuItem[] = [];
  @Input() isAuth: boolean = false;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isAuth = this.authService.isAuth
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let routeName = event.url.split("/")[1]
        // if (routeName === "home") {
        //   this.menuItems = [];
        // } else {
          let firstLetter = routeName.substr(0, 1)
          this.menuItems = [{
            label: routeName.replace(firstLetter, firstLetter.toUpperCase()),
            routerLink: `/${routeName}`
          }];
        // }
      }
    })
  }
}
