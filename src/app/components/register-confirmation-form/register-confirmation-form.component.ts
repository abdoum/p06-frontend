import {Component, OnInit} from '@angular/core';
import {RegisterRequest} from "../../models/login-request";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register-confirmation-form',
  templateUrl: './register-confirmation-form.component.html',
  styleUrls: ['./register-confirmation-form.component.scss']
})
export class RegisterConfirmationFormComponent implements OnInit {
  registrationRequest= new RegisterRequest();

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.registrationRequest = this.authService.registrationRequest;
  }

  onCreate() {
    this.authService.register(this.registrationRequest);
  }

  prevPage() {
    this.router.navigate(['register/bank']);
  }
}
