import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterConfirmationFormComponent } from './register-confirmation-form.component';

describe('RegisterConfirmationFormComponent', () => {
  let component: RegisterConfirmationFormComponent;
  let fixture: ComponentFixture<RegisterConfirmationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterConfirmationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterConfirmationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
