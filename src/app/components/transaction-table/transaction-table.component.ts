import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TransactionService} from "../../services/transaction.service";
import {Subscription} from "rxjs";
import {Transaction} from "../../models/transaction";

@Component({
  selector: 'app-transaction-table',
  templateUrl: './transaction-table.component.html',
  styleUrls: ['./transaction-table.component.scss']
})
export class TransactionTableComponent implements OnInit, OnDestroy {
  transactions: Transaction[] = [];
  transactionsSubscription = new Subscription();
  rows: number = 3;
  first = 0;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.transactionsSubscription = this.transactionService.transactionsSubject.subscribe((transactions) => {
      this.transactions = transactions;
    })
    this.transactionService.emitTransactionsSubject()
    this.transactionService.getAllTransactions()
  }

  ngOnDestroy() {
    this.transactionsSubscription.unsubscribe()
  }

}
