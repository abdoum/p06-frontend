import {Component, OnDestroy, OnInit} from '@angular/core';
import {WalletService} from "../../services/wallet.service";
import {of, Subscription} from "rxjs";
import {Transaction, Wallet} from "../../models/transaction";
import {StatsService} from "../../services/stats.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  data: any;
  chartOptions: any;
  wallet = new Wallet();
  statsSubscription = new Subscription();
  walletSubscription = new Subscription();
  today: number = Date.now();
  lastMonth = new Date().getMonth() - 1
  stats: any = {};

  constructor(private walletService: WalletService,
              private statsService: StatsService) {
  }

  ngOnInit(): void {
    this.walletSubscription = this.walletService.walletSubject.subscribe((wallet) => {
      this.wallet = wallet;
    })
    this.statsSubscription = this.statsService.statsSubject.subscribe(stats => {
      this.stats = stats;
    this.data = {
      labels: ['Sent', 'Received', 'Bank transfer'],
      datasets: [
        {
          data: [this.stats?.sentTransactions, this.stats?.receivedTransactions, this.stats?.bankAccountTransactions],
          backgroundColor: [
            "#03a678",
            "#9370db",
            "#009fd4"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#7659b6",
            "#36A2EB"
          ]
        }
      ]
    };
    this.chartOptions = {
      plugins: {
        title: {
          display: false,
          text: 'Last Month',
          font: {
            size: 20,
            weight: 'bold',
            lineHeight: 1.2,
          },
        },
        legend: {
          position: 'bottom'
        }
      },
    };
    })
    this.walletService.emitWalletsSubject()
    this.walletService.getWallet()
    this.statsService.emitStatsSubject()
    this.statsService.getDashboardStats()
  }

  ngOnDestroy(): void {
    this.walletSubscription.unsubscribe()
    this.statsSubscription.unsubscribe()
  }
}
