import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem, MessageService} from "primeng/api";
import {Subscription} from "rxjs";
import {AuthService} from "../../services/auth.service";
import {StepsModule} from 'primeng/steps';

@Component({
  selector: 'app-register-steps',
  templateUrl: './register-steps.component.html',
  styleUrls: ['./register-steps.component.scss'],
  providers: [MessageService]
})
export class RegisterStepsComponent implements OnInit, OnDestroy {
  items: MenuItem[] = [];
  subscription = new Subscription();
  personalInfos: any;

  constructor(public messageService: MessageService, public authService: AuthService) {
  }

  ngOnInit() {
    this.personalInfos = this.authService.registrationRequest;

    this.items = [{
      label: 'Credentials',
      routerLink: 'credentials'
    },
      {
        label: 'Personal information',
        routerLink: 'personal'
      },
      {
        label: 'Bank account',
        routerLink: 'bank'
      },
      {
        label: 'Confirmation',
        routerLink: 'confirmation'
      }
    ];

    this.subscription = this.authService.registerObjectSubject.subscribe((personalInformation) => {
      this.personalInfos = personalInformation;
      this.messageService.add({
        severity: 'success',
        summary: 'Order submitted',
        detail: 'Dear, ' + personalInformation + ' thank you for joining'
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
