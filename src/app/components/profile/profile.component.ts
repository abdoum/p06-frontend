import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {AuthUser} from "../../models/auth-user";
import {ProfileService} from "../../services/profile.service";
import {Message, MessageService} from "primeng/api";
import {WalletService} from "../../services/wallet.service";
import {BankAccount, Wallet} from "../../models/transaction";
import {BankAccountService} from "../../services/bank-account.service";
import {Subscription} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TransactionService} from "../../services/transaction.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [MessageService]
})
export class ProfileComponent implements OnInit, OnDestroy {
  currentUser: AuthUser | undefined;
  messages: Message[] = [];
  wallet!: Wallet;
  bankAccount: BankAccount = new BankAccount();
  bankAccountSubscription: Subscription = new Subscription;
  walletSubscription = new Subscription();

  constructor(private authService: AuthService,
              private walletService: WalletService,
              private profileService: ProfileService,
              private formBuilder: FormBuilder,
              private transactionService: TransactionService,
              private bankAccountService: BankAccountService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.bankAccountSubscription = this.bankAccountService.bankAccountSubject.subscribe((bankAccount) => {
      this.bankAccount = bankAccount;
    })
    this.bankAccountService.getBankAccountDetails(this.messageService)

    this.walletSubscription = this.walletService.walletSubject.subscribe((wallet)=>{
      this.wallet = wallet;
    })
    this.walletService.getWallet()

    this.profileService.getUserDetails(this.messageService)
    this.currentUser = this.authService.currentUser
  }

  ngOnDestroy() {
    this.bankAccountSubscription.unsubscribe()
  }
}
