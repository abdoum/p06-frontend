import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Message, MessageService} from "primeng/api";
import {TransactionService} from "../../services/transaction.service";
import {BankAccount, Wallet} from "../../models/transaction";
import {AuthService} from "../../services/auth.service";
import {Subscription} from "rxjs";
import {WalletService} from "../../services/wallet.service";

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  transactionForm!: FormGroup;
  messages: Message[] = [];
  wallet!: Wallet;
  walletSubscription = new Subscription();

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private walletService: WalletService,
              private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.walletSubscription = this.walletService.walletSubject.subscribe((wallet) => {
      this.wallet = wallet;
    })
    this.walletService.getWallet()
    this.initForm();
  }

  private initForm() {
    this.transactionForm = this.formBuilder.group({
      receiverUserId: ['', Validators.required],
      amount: ['', Validators.min(1)]
    })
  }

  onSubmitForm() {
    this.transactionForm.setValue({receiverUserId: this.authService.currentUser.id,
      amount: this.wallet?.balance})
    this.transactionService.addTransaction(this.transactionForm);
    this.transactionForm.reset();
  }
}
