import {Component, OnInit} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Subscription} from "rxjs";
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Pay My Buddy';
  isAuth: boolean = false;
  isAuthSubscription: Subscription = new Subscription();

  constructor(private authService : AuthService) { }

  ngOnInit(): void {
    this.isAuth = this.authService.isAuth
    this.isAuthSubscription = this.authService.isAuthSubject.subscribe((isAuth)=> {
      this.isAuth = isAuth;
    })
    this.authService.emitIsAuthSubject()
  }
}
