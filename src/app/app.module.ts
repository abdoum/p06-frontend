import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule, routingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {TransactionTableComponent} from './components/transaction-table/transaction-table.component';
import {TopbarComponent} from './components/topbar/topbar.component';
import {PaginationComponent} from './components/pagination/pagination.component';
import {TransactionFormComponent} from './components/transaction-form/transaction-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LogoComponent} from './components/logo/logo.component';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ProfileComponent} from './components/profile/profile.component';
import {ContactComponent} from './components/contact/contact.component';
import {BreadcrumbModule} from "primeng/breadcrumb";
import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {MessagesModule} from "primeng/messages";
import {MessageModule} from "primeng/message";
import {AuthService} from "./services/auth.service";
import {AuthGuardService} from "./services/auth-guard.service";
import {ContactService} from "./services/contact.service";
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {InputNumberModule} from "primeng/inputnumber";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {RadioButtonModule} from "primeng/radiobutton";
import {DialogModule} from "primeng/dialog";
import {DropdownModule} from "primeng/dropdown";
import {ToolbarModule} from "primeng/toolbar";
import {TreeSelectModule} from "primeng/treeselect";
import {AvatarModule} from "primeng/avatar";
import {ChartModule} from "primeng/chart";
import {TransactionService} from "./services/transaction.service";
import {TransferComponent} from "./components/transfert/transfer.component";
import {RippleModule} from "primeng/ripple";
import {BankTransactionFormComponent} from './bank-transaction-form/bank-transaction-form.component';
import {CardModule} from "primeng/card";
import {WalletComponent} from './profile/wallet/wallet.component';
import {StyleClassModule} from "primeng/styleclass";
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from "@angular/common";
import {RegisterCredentialsForm} from './components/register/register-credentials-form.component';
import {RegisterStepsComponent} from './components/register-steps/register-steps.component';
import {StepsModule} from "primeng/steps";
import {BankAccountFormComponent} from './components/bank-account-form/bank-account-form.component';
import {InputTextModule} from "primeng/inputtext";
import {RegisterPersonalFormComponent} from './components/register-personal-form/register-personal-form.component';
import {RegisterConfirmationFormComponent} from './components/register-confirmation-form/register-confirmation-form.component';
import {CustomHttpInterceptor} from "./custom-http-interceptor";

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    TransactionTableComponent,
    TopbarComponent,
    PaginationComponent,
    TransactionFormComponent,
    LogoComponent,
    LoginComponent,
    HomeComponent,
    routingComponents,
    ProfileComponent,
    TransferComponent,
    ContactComponent,
    BreadcrumbComponent,
    BankTransactionFormComponent,
    WalletComponent,
    RegisterCredentialsForm,
    RegisterStepsComponent,
    BankAccountFormComponent,
    RegisterPersonalFormComponent,
    RegisterConfirmationFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BreadcrumbModule,
    MessagesModule,
    MessageModule,
    ReactiveFormsModule,
    TableModule,
    ButtonModule,
    ToastModule,
    BrowserAnimationsModule,
    InputNumberModule,
    ConfirmDialogModule,
    RadioButtonModule,
    DialogModule,
    DropdownModule,
    ToolbarModule,
    TreeSelectModule,
    AvatarModule,
    ChartModule,
    RippleModule,
    CardModule,
    StyleClassModule,
    StepsModule,
    InputTextModule
  ],
  providers: [AuthService, AuthGuardService, ContactService, TransactionService, {
    provide: LOCALE_ID,
    useValue: 'fr-FR'
  },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
