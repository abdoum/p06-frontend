import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankTransactionFormComponent } from './bank-transaction-form.component';

describe('BankTransactionFormComponent', () => {
  let component: BankTransactionFormComponent;
  let fixture: ComponentFixture<BankTransactionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankTransactionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTransactionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
