import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TransactionService} from "../services/transaction.service";
import {Message, MessageService} from "primeng/api";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-bank-transaction-form',
  templateUrl: './bank-transaction-form.component.html',
  styleUrls: ['./bank-transaction-form.component.scss']
})
export class BankTransactionFormComponent implements OnInit {
  transactionForm!: FormGroup;
  messages: Message[] = [];
  @Input() bankAccount: any = {};
  formVisible = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.transactionForm = this.formBuilder.group({
      receiverUserId: [0, [Validators.required]],
      amount: [1, [Validators.min(1)]],
      isWalletRecharge: [true, [Validators.pattern('true|false')]],
      description: ['', []]
    })
  }

  onSubmitForm() {
    this.transactionForm.setValue({
      receiverUserId: this.authService.currentUser.id,
      amount: this.transactionForm.get('amount')?.value, description: 'Wallet recharge', isWalletRecharge: true
    })
    this.transactionService.addTransaction(this.transactionForm);
    this.initForm();
  }

}
