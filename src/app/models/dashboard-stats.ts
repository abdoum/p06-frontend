export class DashboardStats {
  sentTransactions: string | undefined;
  receivedTransactions: string | undefined;
  bankAccountTransactions: string | undefined;
  connectionsCount: string | undefined;

  constructor(sentTransactions: string | undefined, receivedTransactions: string | undefined,
              bankAccountTransactions: string | undefined, connectionsCount: string | undefined) {
    this.sentTransactions = sentTransactions;
    this.receivedTransactions = receivedTransactions;
    this.bankAccountTransactions = bankAccountTransactions;
    this.connectionsCount = connectionsCount;
  }
}
