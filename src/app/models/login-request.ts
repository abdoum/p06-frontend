import {BankAccount} from "./transaction";

export class LoginRequest {
  username: string | undefined;
  password: string | undefined;
  rememberMe: boolean | undefined;

  constructor(username: string | undefined, password: string | undefined, rememberMe: boolean | undefined) {
    this.username = username;
    this.password = password;
    this.rememberMe = rememberMe;
  }
}

export class RegisterRequest {
  username: string | undefined;
  password: string | undefined;
  passwordConfirmation: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  bankAccount= new BankAccount();
}
