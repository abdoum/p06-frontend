import {User} from "./user.model";

export class BankAccount {
  id: number | undefined;
  iban: string | undefined;
  swiftCode: string | undefined;
  ownerFullName: string | undefined;
  user: User | undefined;
}

export class Transaction {
  id: number | undefined;
  senderWallet: Wallet | undefined;
  receiverWallet: Wallet | undefined;
  date: Date | undefined;
  description: string | undefined;
  amount: number | undefined;
  receiverBankAccount: BankAccount | undefined;

  constructor(senderWallet: Wallet | undefined, receiverWallet: Wallet | undefined, description: string | undefined, amount: number | undefined, receiverBankAccount: BankAccount | undefined) {
    this.senderWallet = senderWallet;
    this.receiverWallet = receiverWallet;
    this.description = description;
    this.amount = amount;
    this.receiverBankAccount = receiverBankAccount;
  }
}

export class Wallet {
  id!: number;
  currency: string = 'EUR';
  balance: number = 0;
  user!: User ;

  constructor() {
  }
}

export class TransactionRequest {
  receiverUserId: number | undefined;
  senderUserId: number | undefined;
  amount: number | undefined;
  description: string | undefined;
  isWalletRecharge: boolean | undefined;

  constructor(senderUserId: any, receiverUserId: number | undefined,
              amount: number | undefined, description: string | undefined, isWalletRecharge: boolean | undefined) {
    this.senderUserId = senderUserId;
    this.receiverUserId = receiverUserId;
    this.amount = amount;
    this.description = description;
    this.isWalletRecharge = isWalletRecharge;
  }
}
