export interface Contact {
  connection_id: number;
  sourceUser: ContactUser;
  targetUser: ContactUser;
}

export interface ContactUser {
  id: number;
  firstName: string;
  lastName: string;
}
