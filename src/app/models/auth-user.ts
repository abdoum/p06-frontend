export class AuthUser {
  id: number | undefined;
  username: string | undefined;
  email: string | undefined;
  roles: string[] | undefined;
  accessToken: string | undefined;
  tokenType: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  constructor() {
  }
}
