import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {LoginComponent} from "./components/login/login.component";
import {AppComponent} from "./app.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {ContactComponent} from "./components/contact/contact.component";
import {TransferComponent} from "./components/transfert/transfer.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {RegisterCredentialsForm} from "./components/register/register-credentials-form.component";
import {RegisterStepsComponent} from "./components/register-steps/register-steps.component";
import {BankAccountFormComponent} from "./components/bank-account-form/bank-account-form.component";
import {RegisterPersonalFormComponent} from "./components/register-personal-form/register-personal-form.component";
import {RegisterConfirmationFormComponent} from "./components/register-confirmation-form/register-confirmation-form.component";

const routes: Routes = [
  {path: "", component: LoginComponent},
  {path: "home", canActivate: [AuthGuardService], component: HomeComponent},
  {path: "transfer", data: {breadcrumb: "transfers"}, canActivate: [AuthGuardService], component: TransferComponent},
  {path: "profile", data: {breadcrumb: "profile"}, canActivate: [AuthGuardService], component: ProfileComponent},
  {path: "contact", data: {breadcrumb: "contact"}, canActivate: [AuthGuardService], component: ContactComponent},
  {path: "login", data: {breadcrumb: "login"}, component: LoginComponent},
  {
    path: "register", data: {breadcrumb: "register-steps"}, component: RegisterStepsComponent, children: [
      {path: "credentials", component: RegisterCredentialsForm},
      {path: "personal", component: RegisterPersonalFormComponent},
      {path: "bank", component: BankAccountFormComponent},
      {path: "confirmation", component: RegisterConfirmationFormComponent}
    ]
  },
  {path: "**", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routingComponents = [HomeComponent, LoginComponent, AppComponent]
