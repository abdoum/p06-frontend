import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {Subject} from "rxjs/Subject";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {Wallet} from "../models/transaction";

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  wallet!: Wallet;
  walletSubject = new Subject<any>();
  errorMessage: string = '';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  emitWalletsSubject() {
    this.walletSubject.next(this.wallet)
  }

  getWallet() {
    const userId = this.authService.currentUser.id || 0
    const params = new HttpParams()
      .set('userId', userId)
    this.http.get<Wallet>(`${environment.hostUrl}/wallet`, {
      params,
      headers: {
        "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
      }
    },)
      .subscribe(
        (res) => {
          this.wallet = res
          this.emitWalletsSubject();
        },
        (res) => {
          this.errorMessage = res.error.message;
        }
      )
  }
}
