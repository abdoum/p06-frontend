import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {LazyLoadEvent, Message, MessageService} from "primeng/api";
import {environment} from "../../environments/environment";
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  errorMessage: Message = {};

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  getUserDetails(messageService: MessageService) {
    this.http.get<User>(`${environment.hostUrl}/profile/${this.authService.currentUser.id}`,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (res) => {
          this.authService.currentUser.firstName = res.firstName
          this.authService.currentUser.lastName = res.lastName
        },
        (_) => {
          messageService.add({
            severity: 'error',
            summary: 'User error',
            detail: 'An Error occurred while retrieving the user details.'
          })
        })
  }

}
