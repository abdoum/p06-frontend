import {Injectable} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";
import {TransactionRequest} from "../models/transaction";
import {FormGroup} from "@angular/forms";
import {Message} from "primeng/api";
import {WalletService} from "./wallet.service";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  transactions: any[] = [];
  transactionsSubject = new Subject<any[]>();
  successMessages: Message[] = [];
  errorMessages: Message[] = [];

  constructor(private http: HttpClient, private authService: AuthService, private walletService: WalletService) {
  }

  emitTransactionsSubject() {
    this.transactionsSubject.next(this.transactions)
  }

  getAllTransactions() {
    this.http.get<any[]>(`${environment.hostUrl}/transactions/${this.authService.currentUser.id}`,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (response) => {
          this.transactions = response;
          this.emitTransactionsSubject();
          this.walletService.getWallet()
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      )
  }

  addTransaction(transactionForm: FormGroup) {
    const formValue = transactionForm.value;
    const newTransaction = new TransactionRequest(
      this.authService.currentUser.id,
      formValue['receiverUserId'],
      formValue['amount'],
      formValue['description'],
      formValue['isWalletRecharge']
    );
    this.http.post(`${environment.hostUrl}/transactions`, newTransaction,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (response) => {
          this.transactions.push(response)
          this.successMessages.push({
            severity: 'success', summary: 'Transfer made!', detail: `You have
     sent ${transactionForm.value['amount']} €`
          })
          this.emitTransactionsSubject();
          this.walletService.getWallet()
        },
        (err) => {
          this.errorMessages.splice(0, this.errorMessages.length)
          this.errorMessages.push({severity: 'error', summary: "Error: ", detail: err.error.message})
          transactionForm.get("amount")?.setErrors({
            serverError: this.errorMessages
          })
        }
      )
  }
}
