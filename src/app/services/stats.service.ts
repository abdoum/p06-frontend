import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";
import {Subject} from "rxjs/Subject";
import {DashboardStats} from "../models/dashboard-stats";

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  stats: any = {};
  statsSubject = new Subject<DashboardStats>();

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  emitStatsSubject() {
    this.statsSubject.next(this.stats)
  }

  getDashboardStats() {
    this.http.get<any>(`${environment.hostUrl}/stats/${this.authService.currentUser.id}`,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (response) => {
          this.stats = response;
          this.emitStatsSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      )
  }
}
