import {Injectable, Optional} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {AuthService} from "./auth.service";
import {Subject} from "rxjs/Subject";
import {LazyLoadEvent, Message} from "primeng/api";
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  contacts: any[] = [];
  newContact: any;
  contactsSubject = new Subject<any[]>();
  newContactSubject = new Subject<any>();
  errorMessage: string = '';
  pageable: any;
  totalElements = 0;
  errorMessages: Message[] = [];

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  emitContactsSubject() {
    this.contactsSubject.next(this.contacts)
  }

  emitNewContactSubject() {
    this.newContactSubject.next(this.newContact)
  }

  getAllContacts(event?: LazyLoadEvent) {
    this.http.get<any[]>(`${environment.hostUrl}/connection/${this.authService.currentUser.id}`,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (res: any) => {
          this.contacts = res;
          this.emitContactsSubject();
        },
        (res) => {
          this.errorMessage = res.error.message;
        }
      )
  }

  addContact(contactForm: FormGroup) {
    this.http.post(`${environment.hostUrl}/connection/${this.authService.currentUser.id}`, contactForm.value['email'],
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (res) => {
          this.contacts.push(res)
          this.emitContactsSubject();
          this.emitNewContactSubject()
        },
        (err) => {

          this.errorMessage = err.error.message;
          this.errorMessages.splice(0, this.errorMessages.length)
          this.errorMessages.push({severity: 'error', summary: "Error: ", detail: err.error.message});
          contactForm.get("email")?.setErrors({
            serverError: this.errorMessages
          })
        }
      )
  }
}
