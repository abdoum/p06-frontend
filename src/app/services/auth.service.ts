import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Message} from "primeng/api";
import {Subject} from "rxjs/Subject";
import {AuthUser} from "../models/auth-user";
import {LoginRequest, RegisterRequest} from "../models/login-request";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  registerObjectSubject = new Subject();
  isAuthSubject = new Subject<boolean>();
  errorMessage = 'Invalid username or password';
  successMessage = 'Login successful!!';
  messages: Message[] = [];
  isAuth = true;
  currentUser = new AuthUser();
  private _registrationRequest = new RegisterRequest();

  get registrationRequest(): RegisterRequest {
    return this._registrationRequest;
  }

  set registrationRequest(value: RegisterRequest) {
    this._registrationRequest = value;
    this.emitRegisterObject()
  }

  constructor(private http: HttpClient, private router: Router) {
  }

  emitIsAuthSubject() {
    this.isAuthSubject.next(this.isAuth)
  }

  emitRegisterObject() {
    this.registerObjectSubject.next(this.registrationRequest)
  }

  login(loginForm: FormGroup) {
    const formValue = loginForm.value;
    const newLogin = new LoginRequest(formValue['username'], formValue['password'], formValue['rememberMe']);
    this.http.post(environment.hostUrl + "/login", newLogin)
      .subscribe((res) => {
          if (res.hasOwnProperty("accessToken")) {
            this.handleSuccessfulLogin(res);
          }
        }, (err) => {
          this.handleFailedLogin(loginForm, err);
        }
      );
  }

  register(registrationRequest: RegisterRequest) {
    let loginForm: FormBuilder;
    loginForm = new FormBuilder();
    const formValue = loginForm.group({
      username: [this.registrationRequest.username, [Validators.required, Validators.email]],
      password: [this.registrationRequest.password, [Validators.required, Validators.pattern('[A-Za-z\\d]{6,25}')]],
      rememberMe: [false, [Validators.pattern('true|false')]]
    });
    this.http.post(environment.hostUrl + "/register", this._registrationRequest)
      .subscribe((res) => {
          this.handleSuccessfulLogin(res);
        }, (err) => {
          this.handleFailedLogin(formValue, err);
        }
      );
  }

  private handleFailedLogin(loginForm: FormGroup, err: Object) {
    this.messages = [{severity: 'error', summary: 'Login failed', detail: this.errorMessage}]
    loginForm.get("username")?.setErrors({
      serverError: this.messages
    })
  }

   handleSuccessfulLogin(res: any) {
    this.currentUser.id = res.id;
    this.currentUser.username = res.username;
    this.currentUser.email = res.email;
    this.currentUser.roles = res.roles;
    this.currentUser.accessToken = res.accessToken;
    localStorage.setItem('token', res.accessToken)
    this.currentUser.tokenType = res.tokenType;
    this.isAuth = true;
    this.messages.push({severity: 'success', summary: 'Success', detail: this.successMessage});
    this.emitIsAuthSubject()
    // this.router.navigate(["/home"]);
    this.router.navigate(["/profile"]);
    this.messages = [];
  }

  logOff() {
    localStorage.removeItem('token')
    this.isAuth = false;
    this.emitIsAuthSubject()
    this.router.navigate(["/login"])
  }


}
