import { Injectable } from '@angular/core';
import {Message, MessageService} from "primeng/api";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";
import {Subject} from "rxjs/Subject";

@Injectable({
  providedIn: 'root'
})
export class BankAccountService {
  errorMessage: Message = {};
  bankAccountSubject = new Subject<any>();
  bankAccount = {};

  constructor(private http: HttpClient, private authService: AuthService) {
  }
  emitBankAccountSubject() {
    this.bankAccountSubject.next(this.bankAccount)
  }
  getBankAccountDetails(messageService: MessageService) {
    this.http.get<any>(`${environment.hostUrl}/bank-account/${this.authService.currentUser.id}`,
      {
        headers: {
          "Authorization": `Bearer ${this.authService.currentUser.accessToken}`
        }
      })
      .subscribe(
        (res) => {
          this.bankAccount = res;
          this.emitBankAccountSubject()
          return res
        },
        (_) => {
          messageService.add({
            severity: 'error',
            summary: 'User error',
            detail: 'An Error occurred while retrieving bank account details.'
          })
        })
  }
}
