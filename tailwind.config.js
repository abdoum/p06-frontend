const {content} = require("tailwindcss/lib/plugins");
require('dotenv').config();
const enablePurge = process.env.ENABLE_PURGE || false
module.exports = {
  purge: [],
  purge: {
    enabled: enablePurge,
    content: [
      './src/**/*.html',
      './src/**/*.scss',
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: '2rem'
    },
    extend: {
      colors: {
          'yellow-light': '#feffe0',
          'yellow-dark': '#FFE5AB',
      },
    },
  },
  variants: {
    extend: {
      opacity: ['disabled']
    },
  },
  plugins: [],
}
